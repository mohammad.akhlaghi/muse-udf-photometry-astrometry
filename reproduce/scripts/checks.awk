# checks.awk: Library of checks for awk programs.
#
# These are some functions that can be used in any awk script to check
# if the necessary variables are set and if they have certain
# properties (for example being an integer). This library can be
# included into any AWK script using the -i option:
#
# awk -v var1=value1 -v var2=value2 -i this/library -f prog.awk
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# checks.awk is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# checks.awk is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Check if the variable is set (not equal to the empty string).
function checkifset(variable, varname)
{
    if(variable==""){
        print varname " is not set!" > "/dev/stderr"
        exit 1
    }
}





# Check if the variable is an integer (mainly for column variables).
function checkifint(variable, varname)
{
    if(variable!=int(variable)){
        printf ("%s (%s) is not an integer!\n", varname, variable) \
            > "/dev/stderr"
        exit 1
    }
}
