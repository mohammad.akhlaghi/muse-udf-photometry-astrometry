# Average and standard deviation of Y values in X axis bins.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.
#
#
#
#
# This script goes over the given input column and counts how many of
# the data elements (rows) are within the given range.
#
# Library functions:
# ==================
#
# This program needs two outside functions:
#
# checkifset()
# checkifint()
#
#
# Variables:
# ==========
#
# xmin:           Minimum value for the histogram.
# xmax:           Maximum value for the histogram.
# xnumbins:       Number of bins in the histogram.
# xcol:           Column representing X axis.
# ycol:           Column representing Y axis.
# showemptylast:  If ==1, then show the last bins if
#                    they empty (with a zero value).
# extraout:       Add an extra empty bin on each side.
#
# Example:
# ========
#
# Lets assume you have put all the variables into the shell variable
# AWKVAR. So you can run this script with:
#
# awk $AWKVAR -i /path/to/checks.awk -f /path/to/histogram.awk input.txt




# Do the preparations:
BEGIN {
    # Check if all the variables are set:
    checkifset(xmax, "xmax")
    checkifset(xmin, "xmin")
    checkifint(xcol, "xcol")
    checkifint(ycol, "ycol")
    checkifint(xnumbins, "xnumbins")
    checkifset(stdmultip, "stdmultip")

    # Set the optional parameters.
    if(extraout=="") extraout=0

    # Set the required variables (macros)
    xwidth=(xmax-xmin)/xnumbins
}




# Go over each line (record) in the input file. If it is commented
# (starts with a #) then ignore it. This histogram finding method uses
# the special feature of AWK arrays: that the indexs have names, they
# are not ordered, they can be added any time and they initiate with
# zero. For each field checked, the appropriate counter is incremented
# by one.
!/^#/ {

    # If the value is smaller or larger than the desired region, then
    # go to the next record. This is not of interest.
    if($xcol < xmin || $xcol > xmax) next

    # Find the middle point of the bin point for record.
    xmidbin = xmin + int( ($xcol-xmin)/xwidth )*xwidth + xwidth/2

    # If $xcol==xmax then the midbins will be larger than the maximum
    # value. In that case, we want it to be added to the last bin.
    if(xmidbin>xmax) {
        if($xcol==xmax) xmidbin-=xwidth
        else next
    }

    # Convert the midbin value to a string, so floating point errors
    # don't bother with the counting.
    strmidbin=sprintf("%-10.3f", xmidbin)

    # Increment the number, add the value to the array.
    values[strmidbin][num[strmidbin]] = $ycol
    num[strmidbin]++
}





# Print out the results.
END {
    # Set the first and last bins.
    minix = extraout==0 ? 0        : -1;
    maxix = extraout==0 ? xnumbins : xnumbins+1;

    for(x=minix; x<maxix; ++x){

	# Set the string value of this bin (to read from the array).
	strmidbin=sprintf("%-10.3f", xmin+x*xwidth+xwidth/2)

	# Only go through the trouble if we have more than two
	# elements.
	if(num[strmidbin]<=2) continue

	# Since there are strong outliers sometimes, we need to
	# sigma-clip the data, So first find the initial standard
	# deviation.
	n=0
	M2=0
	imean=0
	for(i=0;i<num[strmidbin];++i) {
	    # Standard deviation is found using the Welford algorithm
	    # given in Wikipedia:
	    # (https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance).
	    ++n;
	    print values[strmidbin][i]
	    delta = values[strmidbin][i] - imean
	    imean += delta / n
	    delta2 = values[strmidbin][i] - imean
	    M2 += delta * delta2
	}
	istd=M2/(n-1)

	printf("%s: %d, %f, %f\n", strmidbin, n, imean, istd);

	# Now, find the mean and STD again, but only with values that
	# are within a given multiple of the STD.
	n=0
	M2=0
	mean=0
	for(i=0;i<num[strmidbin];++i)
	    if(values[strmidbin][i]>imean-stdmultip*istd               \
	       && values[strmidbin][i]<imean+stdmultip*istd) {
		++n;
		delta = values[strmidbin][i] - mean
		mean += delta / n
		delta2 = values[strmidbin][i] - mean
		M2 += delta * delta2
	    }

	printf("%s: %d, %f, %f\n", strmidbin, n, mean, n>2 ? M2/(n-1) : 0);

	# Print this value if we have more than two elements:
	#if(n>2)
	#printf("%-10.3f%-10.3f%-10.3f\n", xmin+x*xwidth+xwidth/2,
	#       mean, M2/(n-1) )
    }
}
