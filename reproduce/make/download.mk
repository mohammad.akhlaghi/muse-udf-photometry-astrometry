# Download all the necessary input images if they are not already
# present in the specified directories. Note that downloading is
# essentially a serial (not parallel) operation (you have one input
# into the network), so the recipes in this Makefile all use a file
# lock to have one download script running at every instant.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.




# Downloader and lock
# -------------------
#
# See if the downloader tools are ready and set them
# accordingly. Afterwards, define the lock directory for operations
# that must not be done in parallel.
locks = $(BDIR)/locks
downloader := $(shell if type curl > /dev/null; then downloader="curl -o";  \
	              else                           downloader="wget -O";  \
	              fi; echo "$$downloader";                               )





# MUSE pseudo-narrow-band images
# ------------------------------
#
# The MUSE pseudo-narrow-band images are hosted on Zenodo within a
# single tarball. So, in a single rule, we will download the tarball
# into the directory that will contain all the files, then unpack the
# tarball. All the files depend on that single rule, so after its
# finished it will just make a text file informing Make that it is
# done.
musetz = muse-pseudo-broadband-images.tar.gz
musetzurl = https://zenodo.org/record/1163746/files
museinputready = $(MUSEINPUTS)/downloaded-unpacked.txt
allmuseinputs = $(foreach u, udf udf10,                           \
                  $(foreach f, $(filters), $(MUSEINPUTS)/muse-$(u)-$(f).fits) )

$(MUSEINPUTS):; mkdir $@
$(allmuseinputs): $(museinputready)
$(museinputready): | $(MUSEINPUTS)

        # Download the tarball (with a lock).
	flock $(locks)/download -c                                           \
	      "$(downloader) $(MUSEINPUTS)/$(musetz) $(musetzurl)/$(musetz)"

        # Unpack the tarball (into the current directory).
	cd $(MUSEINPUTS); tar xf $(MUSEINPUTS)/$(musetz) --strip-components=1

        # Write the target: we can't rely on an individual file to be
        # the target because the download and unpacking might for some
        # reason break up in the middle and some files will be left
        # undone.
	echo "files downloaded and unpacked" > $@





# XDF
# ---
xdfweb = https://archive.stsci.edu/pub/hlsp/xdf
xdfacspre = hlsp_xdf_hst_acswfc-$(hstpixelscale)0mas_hudf_
allxdf = $(foreach f, $(filters), $(XDF)/$(xdfacspre)$(f)_v1_sci.fits)
$(XDF):; mkdir $@
$(allxdf): $(XDF)/%: | $(XDF) $(locks)
	flock $(locks)/download -c "$(downloader) $@ $(xdfweb)/$*"





# Filter throughputs
# ------------------
throughputsweb = http://www.stsci.edu/hst/acs/analysis/throughputs/tables
filtersup := $(shell for f in $(filters); do echo $$f | tr a-z A-Z; done)
throughputs = $(foreach F, $(filtersup), $(TROUGHPUTS)/wfc_$(F).dat)
$(TROUGHPUTS):; mkdir $@
$(throughputs): $(TROUGHPUTS)/%: | $(TROUGHPUTS) $(locks)
	flock $(locks)/download -c "$(downloader) $@ $(throughputsweb)/$*"
