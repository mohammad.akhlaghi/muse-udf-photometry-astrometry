# Broad-band photometry checks with MUSE generated broad-band images.
#
# The rules in this file are for measuring the statistics over each
# field to compare.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.



# Prepare 2D histograms
# ---------------------
#
# For time being, we are plotting 2D histograms.
twodxmin=20
twodxmax=30
twodxnbins=40
twodynbins=42
twodmagmin=-1.8125
twodmagmax=1.8125
twoddistmin=-0.4
twoddistmax=0.4
twoddir = $(BDIR)/tex/2D-histograms
scripts2d = reproduce/scripts/two-dim-hist.awk reproduce/scripts/checks.awk

mag2d = $(foreach field, udf udf10,                                       \
	  $(foreach filter, $(filters), $(twoddir)/$(field)-$(filter)-mag.txt) )
$(mag2d): $(twoddir)/%-mag.txt: $(mcatdir)/%.txt $(scripts2d) | $(twoddir)

	awk -vxcol=5   -vxmin=$(twodxmin)     -vxmax=$(twodxmax)          \
	    -vycol=8   -vymin=$(twodmagmin)   -vymax=$(twodmagmax)        \
	    -vxnumbins=$(twodxnbins) -vynumbins=$(twodynbins)             \
	    -vshowemptylast=1 -vextraout=0 -ireproduce/scripts/checks.awk \
	    -freproduce/scripts/two-dim-hist.awk $< > $@;



radec2d = $(foreach field, udf udf10,                                       \
	    $(foreach filter, $(filters),                                   \
	      $(foreach d, ra dec, $(twoddir)/$(field)-$(filter)-$(d).txt) ) )
$(radec2d): $(twoddir)/%.txt: $(acatdir)/$$(word 1,$$(subst -, , $$*))-$$(word 2,$$(subst -, , $$*)).txt $(scripts2d) | $(twoddir)

        # Pull out the magnitude and distance in arcseconds.
	if [ $(word 3,$(subst -, , $*)) = ra ]; then                      \
	  awk '!/^#/{print $$5, 3600*$$6}' $< > $(@D)/$*-tmp.txt;         \
	else                                                              \
	  awk '!/^#/{print $$5, 3600*$$7}' $< > $(@D)/$*-tmp.txt;         \
	fi

        # Make the 2D histogram.
	awk -vxcol=1   -vxmin=$(twodxmin)     -vxmax=$(twodxmax)          \
	    -vycol=2   -vymin=$(twoddistmin)  -vymax=$(twoddistmax)       \
	    -vxnumbins=$(twodxnbins) -vynumbins=$(twodynbins)             \
	    -vshowemptylast=1 -vextraout=0 -ireproduce/scripts/checks.awk \
	    -freproduce/scripts/two-dim-hist.awk $(@D)/$*-tmp.txt > $@


        # Clean up.
	rm $(@D)/$*-tmp.txt





# Average and dispersion in bins
# ------------------------------
#
# To confirm that there is no problem in the individual fields, we
# also need the average and standard deviation in each bin of each
# field to plot as lines over the 2D histograms.
histxnbins = 15
histstdmultip = 3
statdir = $(BDIR)/statistics
mag-ave-std = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                    \
	        $(foreach f, $(filters), $(statdir)/udf$(uid)-$(f).txt) )
$(mag-ave-std): $(statdir)/%.txt: $(ccatdir)/%-mag.txt                \
	        reproduce/scripts/bin-ave-std.awk | $(statdir)

	awk -vxmin=$(twodxmin) -vxmax=$(twodxmax) -vxcol=5 -vycol=8   \
	    -vxnumbins=$(twodxnbins) -vstdmultip=$(histstdmultip)     \
	    -i reproduce/scripts/checks.awk                           \
	    -f reproduce/scripts/bin-ave-std.awk  $<

	exit 1





# TeX macros
# ----------
$(mtexdir)/statistics.tex: $(two-d-hists) | $(mtexdir)
	echo "\\newcommand{\\twodxmin}{$(twodxmin)}"             > $@
	echo "\\newcommand{\\twodxmax}{$(twodxmax)}"            >> $@
	echo "\\newcommand{\\twodxnbins}{$(twodxnbins)}"        >> $@
	echo "\\newcommand{\\twodynbins}{$(twodynbins)}"        >> $@
	echo "\\newcommand{\\histstdmultip}{$(histstdmultip)}"  >> $@
