# Broad-band photometry checks with MUSE generated broad-band images.
#
# Degrade the HST images to MUSE PSF and resolution
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.





# Throughput effective wavelength
# -------------------------------
#
# We need one wavelength to generate the parametric MUSE PSF. So we
# will take the throughput-weighted average wavelength for the
# job. Unfortunately in the STSCI throughput filenames, there letters
# are all upper-case. So we have to deal with the throughputs
# separately.
leffdir=$(BDIR)/lambda-eff
lambda-eff = $(leffdir)/$(lastword $(filters)).txt
$(lambda-eff): $(throughputs) | $(leffdir)

        # Find the throughput weighted average for each filter.
	for f in $(filters); do                                         \
	  awk '{sum+=$$1*$$2; sumw+=$$2;} END{print(sum/sumw)}'         \
	      $(TROUGHPUTS)/wfc_$$(echo $$f | tr a-z A-Z).dat           \
	      > $(leffdir)/$$f.txt;                                     \
	done





# MUSE PSF information from headers
# ---------------------------------
#
# The MUSE PSF is given in terms of Moffat function parameters in the
# headers of the input images. So we have to pull those parameters out
# first.
mpsfdir = $(BDIR)/muse-psfs
all-muse-udf-psfs = $(foreach f, $(filters), $(MUSEINPUTS)/muse-udf-$(f).fits) \
	     	    $(foreach f, $(filters), $(MUSEINPUTS)/muse-udf10-$(f).fits)
$(mpsfdir)/muse-psfs.txt: $(all-muse-udf-psfs) $(lambda-eff)                   \
	                  reproduce/config/internal/truncation-muse-psf.mk     \
	                  reproduce/config/internal/hst-pixel-scale.mk         \
	                  | $(mpsfdir)

        # Delete the output file because we will be appending to it,
        # so if it already exists, there is going to be trouble.
	rm -f $@

        # Write the information for each PSF in the file. The
        # MakeProfiles columns are defined in its configuration file.
	for uid in udf udf10; do                                        \
	  for f in $(filters); do                                       \
            file=$(MUSEINPUTS)/muse-$$uid"-"$$f".fits";                 \
	    lambda=$$(cat $(leffdir)/$$f.txt);                          \
	    astheader $$file | grep FSF |                               \
	      awk 'BEGIN{print "#", "'$$file'"; bc=0; Ac=0; Bc=0;}      \
	           /FSF0.BET/ {beta[bc++]=$$NF}                         \
	           /FSF0.FWA/ {A[Ac++]=$$NF}                            \
	           /FSF0.FWB/ {B[Bc++]=$$NF}                            \
	           END{                                                 \
	            topixel=1/(0.01*$(hstpixelscale));                  \
	            for(i=0;i<bc;++i)                                   \
	               printf("1 0 0 1 %.3f %.3f 0 1 0 %.3f\n",         \
	                      (B[i]*'$$lambda'+A[i])*topixel, beta[i],  \
	                      $(truncation-muse-psf));                  \
	           }' >> $@;                                            \
	  done;                                                         \
	done





# Make the MUSE PSFs
# ------------------
#
# The random number generator and seed must be fixed to get exactly
# the same results. For the seed, I just typed in some numbers to make
# a long integer. The important thing is that it be fixed, its
# absolute value is irrelevant.
muse-rawpsfs = $(mpsfdir)/udf10-f850lp-rawpsf.fits
$(muse-rawpsfs): $(mpsfdir)/muse-psfs.txt $(mkprof)

        # Generate all the PSFs using MakeProfiles. Note that
	export GSL_RNG_TYPE="mt19937";                             \
	export GSL_RNG_SEED=129312398;                             \
	astmkprof $< --individual --output=$(@D) --oversample=1 --nomerged

        # Correct the names to a managable standard. You can see the
        # commented lines in $(dhstdir)/muse-psfs.txt to check them.
	mv $(@D)/0_muse-psfs.fits  $(@D)/udf1-f606w-rawpsf.fits
	mv $(@D)/1_muse-psfs.fits  $(@D)/udf2-f606w-rawpsf.fits
	mv $(@D)/2_muse-psfs.fits  $(@D)/udf3-f606w-rawpsf.fits
	mv $(@D)/3_muse-psfs.fits  $(@D)/udf4-f606w-rawpsf.fits
	mv $(@D)/4_muse-psfs.fits  $(@D)/udf5-f606w-rawpsf.fits
	mv $(@D)/5_muse-psfs.fits  $(@D)/udf6-f606w-rawpsf.fits
	mv $(@D)/6_muse-psfs.fits  $(@D)/udf7-f606w-rawpsf.fits
	mv $(@D)/7_muse-psfs.fits  $(@D)/udf8-f606w-rawpsf.fits
	mv $(@D)/8_muse-psfs.fits  $(@D)/udf9-f606w-rawpsf.fits

	mv $(@D)/9_muse-psfs.fits  $(@D)/udf1-f775w-rawpsf.fits
	mv $(@D)/10_muse-psfs.fits $(@D)/udf2-f775w-rawpsf.fits
	mv $(@D)/11_muse-psfs.fits $(@D)/udf3-f775w-rawpsf.fits
	mv $(@D)/12_muse-psfs.fits $(@D)/udf4-f775w-rawpsf.fits
	mv $(@D)/13_muse-psfs.fits $(@D)/udf5-f775w-rawpsf.fits
	mv $(@D)/14_muse-psfs.fits $(@D)/udf6-f775w-rawpsf.fits
	mv $(@D)/15_muse-psfs.fits $(@D)/udf7-f775w-rawpsf.fits
	mv $(@D)/16_muse-psfs.fits $(@D)/udf8-f775w-rawpsf.fits
	mv $(@D)/17_muse-psfs.fits $(@D)/udf9-f775w-rawpsf.fits

	mv $(@D)/18_muse-psfs.fits $(@D)/udf1-f814w-rawpsf.fits
	mv $(@D)/19_muse-psfs.fits $(@D)/udf2-f814w-rawpsf.fits
	mv $(@D)/20_muse-psfs.fits $(@D)/udf3-f814w-rawpsf.fits
	mv $(@D)/21_muse-psfs.fits $(@D)/udf4-f814w-rawpsf.fits
	mv $(@D)/22_muse-psfs.fits $(@D)/udf5-f814w-rawpsf.fits
	mv $(@D)/23_muse-psfs.fits $(@D)/udf6-f814w-rawpsf.fits
	mv $(@D)/24_muse-psfs.fits $(@D)/udf7-f814w-rawpsf.fits
	mv $(@D)/25_muse-psfs.fits $(@D)/udf8-f814w-rawpsf.fits
	mv $(@D)/26_muse-psfs.fits $(@D)/udf9-f814w-rawpsf.fits

	mv $(@D)/27_muse-psfs.fits $(@D)/udf1-f850lp-rawpsf.fits
	mv $(@D)/28_muse-psfs.fits $(@D)/udf2-f850lp-rawpsf.fits
	mv $(@D)/29_muse-psfs.fits $(@D)/udf3-f850lp-rawpsf.fits
	mv $(@D)/30_muse-psfs.fits $(@D)/udf4-f850lp-rawpsf.fits
	mv $(@D)/31_muse-psfs.fits $(@D)/udf5-f850lp-rawpsf.fits
	mv $(@D)/32_muse-psfs.fits $(@D)/udf6-f850lp-rawpsf.fits
	mv $(@D)/33_muse-psfs.fits $(@D)/udf7-f850lp-rawpsf.fits
	mv $(@D)/34_muse-psfs.fits $(@D)/udf8-f850lp-rawpsf.fits
	mv $(@D)/35_muse-psfs.fits $(@D)/udf9-f850lp-rawpsf.fits

	mv $(@D)/36_muse-psfs.fits $(@D)/udf10-f606w-rawpsf.fits
	mv $(@D)/37_muse-psfs.fits $(@D)/udf10-f775w-rawpsf.fits
	mv $(@D)/38_muse-psfs.fits $(@D)/udf10-f814w-rawpsf.fits
	mv $(@D)/39_muse-psfs.fits $(@D)/udf10-f850lp-rawpsf.fits





# Cutout the MUSE PSFs to the proper size
# ---------------------------------------
#
# The size of the images created by MakeProfiles was based on the PSF
# FWHM. But to match with the HST PSF, we need a different size (set
# in the reproduction pipeline configurations). So here, we will crop
# the desired region from the raw psfs.
muse-psfs = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                             \
              $(foreach f, $(filters), $(mpsfdir)/udf$(uid)-$(f).fits)  )
$(muse-psfs): %.fits: $(muse-rawpsfs)                                        \
	      reproduce/config/internal/psf-match-width.mk

        # First do a sanity check, since the size is fixed. Note that
        # the image sizes are always an odd number, and since AWK does
        # all internal arithmetic in floating point, we will just
        # subtract one after dividing by two to get the central point.
	xyc=$$(astheader $*-rawpsf.fits | grep NAXIS                         \
	                 | awk 'NR==2{xw=$$3} NR==3{yw=$$3}                  \
	                 END{printf "--xc=%d --yc=%d", xw/2+1, yw/2+1}');    \
	astimgcrop $*-rawpsf.fits $$xyc -o$@ --zeroisnotblank                \
	           --iwidth=$(psf-match-width)





# Original HST images of each region
# ----------------------------------
#
# Cutout the original HST images.
hdegdir = $(BDIR)/hst-degrade
xdf-prefix = $(XDF)/$(xdfacspre)
udf1-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf1-$(f)-o.fits)
$(udf1-hst-cutouts): $(hdegdir)/udf1-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
	             $(imgcrop) reproduce/config/internal/vertices-udf1.mk    \
	              | $(hdegdir)
	astimgcrop $(udf1polygon) $< -o$@

udf2-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf2-$(f)-o.fits)
$(udf2-hst-cutouts): $(hdegdir)/udf2-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
	             $(imgcrop) reproduce/config/internal/vertices-udf2.mk    \
                     | $(hdegdir)
	astimgcrop $(udf2polygon) $< -o$@

udf3-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf3-$(f)-o.fits)
$(udf3-hst-cutouts): $(hdegdir)/udf3-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
	             $(imgcrop) reproduce/config/internal/vertices-udf3.mk    \
	              | $(hdegdir)
	astimgcrop $(udf3polygon) $< -o$@

udf4-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf4-$(f)-o.fits)
$(udf4-hst-cutouts): $(hdegdir)/udf4-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
                     $(imgcrop) reproduce/config/internal/vertices-udf4.mk    \
                     | $(hdegdir)
	astimgcrop $(udf4polygon) $< -o$@

udf5-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf5-$(f)-o.fits)
$(udf5-hst-cutouts): $(hdegdir)/udf5-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
                     $(imgcrop) reproduce/config/internal/vertices-udf5.mk    \
                     | $(hdegdir)
	astimgcrop $(udf5polygon) $< -o$@

udf6-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf6-$(f)-o.fits)
$(udf6-hst-cutouts): $(hdegdir)/udf6-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
	             $(imgcrop) reproduce/config/internal/vertices-udf6.mk    \
	             | $(hdegdir)
	astimgcrop $(udf6polygon) $< -o$@

udf7-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf7-$(f)-o.fits)
$(udf7-hst-cutouts): $(hdegdir)/udf7-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
	             $(imgcrop) reproduce/config/internal/vertices-udf7.mk    \
	             | $(hdegdir)
	astimgcrop $(udf7polygon) $< -o$@

udf8-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf8-$(f)-o.fits)
$(udf8-hst-cutouts): $(hdegdir)/udf8-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
	             $(imgcrop) reproduce/config/internal/vertices-udf8.mk    \
	             | $(hdegdir)
	astimgcrop $(udf8polygon) $< -o$@

udf9-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf9-$(f)-o.fits)
$(udf9-hst-cutouts): $(hdegdir)/udf9-%-o.fits: $(xdf-prefix)%_v1_sci.fits     \
	             $(imgcrop) reproduce/config/internal/vertices-udf9.mk    \
	             | $(hdegdir)
	astimgcrop $(udf9polygon) $< -o$@

udf10-hst-cutouts = $(foreach f, $(filters), $(hdegdir)/udf10-$(f)-o.fits)
$(udf10-hst-cutouts): $(hdegdir)/udf10-%-o.fits: $(xdf-prefix)%_v1_sci.fits   \
	              $(imgcrop) reproduce/config/internal/vertices-udf10.mk  \
	              | $(hdegdir)
	astimgcrop $(udf10polygon) $< -o$@





# HST PSFs
# --------
#
# Crop out one star from the HST image, center it and then use it for
# the HST PSF.
hpsfdir=$(BDIR)/hst-psfs
hst-star-cats = $(foreach f, $(filters), $(hpsfdir)/star-$(f).txt)
$(hst-star-cats): $(hpsfdir)/star-%.txt: $(xdf-prefix)%_v1_sci.fits         \
	          reproduce/config/internal/hst-star-position.mk            \
	          reproduce/config/internal/star-cat-threshold.mk           \
	          $(imgcrop) $(noisechisel) $(mkcatalog) | $(hpsfdir)

        # Crop the region in the vicinity of the star in a large
        # enough box that the Sky can be accurate calculated. A width
        # of 15 arcseconds corresponds to a 500x500 box.
	astimgcrop --ra=$(hst-star-ra) --dec=$(hst-star-dec) --wwidth=15    \
	           $< -o$(@D)/$*-l.fits

        # Run NoiseChisel on the image. We don't care about low
        # surface brightness regions here, we just want the high S/N
        # star regions, so we are setting really loose parameters
        # which are necessary for some stars in the 0.06''/pixel
        # images.
	astnoisechisel $(@D)/$*-l.fits --minbfrac=0.2 --minmodeq=0.48       \
	               --detsnminarea=5 --segsnminarea=5

        # Make a catalog. Note that since we want the center of the
        # star to be measured accurately, we use a very large
        # threshold. We are just getting the area
	astmkcatalog $(@D)/$*-l_labeled.fits                                \
                     --area --y --x --threshold=$(star-cat-threshold)

        # The star should be in the central position: the object
        # closest to the coordinates (250,250). Since this is a bright
        # star, we don't expect any star within a few pixels.
	awk '!/^#/ && $$2>123 && $$2<127 && $$3>123 && $$3<127              \
	        {print $$2, $$3}' $(@D)/$*-l_labeled_c.txt > $@

        # Clean up and set final name
	rm $(@D)/$*-l_labeled.fits
	rm $(@D)/$*-l_labeled_o.txt $(@D)/$*-l_labeled_c.txt





# Using the central position of the star calculated from the catalog,
# warp the image into a grid with the star center exactly in the
# center of a pixel
hst-centered-star = $(foreach f, $(filters), $(hpsfdir)/star-$(f).fits)
$(hst-centered-star): $(hpsfdir)/star-%.fits: $(hpsfdir)/star-%.txt         \
	              reproduce/config/internal/psf-match-width.mk

        # Generate the proper options for ImageWarp, then run it. As
        # an example, when the position along an axis is 250.479, we
        # need to translate the grid by 250-250.479 = -0.479 so the
        # center of the star moves to the center of the pixel.
	iwopts=$$(awk '{printf("--translate=%.3f,%.3f", 125-$$1, 125-$$2)}' \
	              $<);                                                  \
	astimgwarp $(@D)/$*-l.fits $$iwopts -o$(@D)/$*-w.fits

        # Crop the centeral few pixels of the image to exactly
        # pinpoint the center.
	astimgcrop --xc=125 --yc=125 $(@D)/$*-w.fits --iwidth=5
	astconvertt $(@D)/$*-w_crop.fits -o$(@D)/$*-c.txt
	rm $(@D)/$*-w_crop.fits

        # Identify the peak pixel.
	xyc=$$(awk 'BEGIN{max=-9e15}                                        \
	            !/^#/{++rc;                                             \
	              for(cc=1;cc<=NF;++cc)                                 \
	              if($$cc>max) {max=$$cc; xc=cc; yc=rc;}                \
                    }                                                       \
	            END{printf "--xc=%d --yc=%d", 125+xc-3, 125+yc-3}'      \
	           $(@D)/$*-c.txt);                                         \
	astimgcrop $$xyc $(@D)/$*-w.fits --iwidth=$(psf-match-width) -o$@

        # Clean up
	rm $(@D)/$*-c.txt $(@D)/$*-w.fits





# Kernels to match HST with MUSE
# ------------------------------
#
# Using the PSF images for MUSE and HST, now we want to find the
# kernel that the HST images should be convolved with to have similar
# PSF to MUSE.
kerneldir = $(BDIR)/kernels
kernels = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                             \
             $(foreach f, $(filters), $(kerneldir)/udf$(uid)-$(f).fits)  )
$(kernels): $(kerneldir)/udf%.fits: $(mpsfdir)/udf%.fits                   \
	    $(hst-centered-star) | $(kerneldir)

        # Get the HST image name and find the kernel
	filter=$(word 2, $(subst -, , $*));                                \
	astconvolve --kernel=$(hpsfdir)/star-$$filter".fits" $<            \
                    --makekernel=15 --viewfreqsteps -o$@





# Convolve HST images with proper PSF
# -----------------------------------
#
# The original HST cutouts are currently in their full resolution, so
# we need to convolve them with the MUSE PSFs (which were made in the
# HST resolution). Note that we will use spatial convolution because
# of NaN values and also edge effects. We are also running convolve on
# one thread so the multiple running on multiple threads can speed
# things up better.
hst-convolved = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                         \
	          $(foreach f, $(filters), $(hdegdir)/udf$(uid)-$(f)-c.fits) )
$(hst-convolved): $(hdegdir)/%-c.fits: $(hdegdir)/%-o.fits $(kerneldir)/%.fits \
	          $(convolve)

        # Convolve the Sky subtracted image.
	astconvolve --spatial $< --kernel=$(kerneldir)/$*.fits -o$@ -N1





# Pixel scale conversion
# ----------------------
#
# For the final degrading of HST images, we also need the scaling
# factor between the HST and MUSE images, so read the MUSE pixel
# resolution from one of the aligned images, then also read the HST
# pixel resolution from one of the images and find scale factor
$(hdegdir)/pix-res-scale.txt: $(MUSEINPUTS)/muse-udf10-f606w.fits          \
                              $(hdegdir)/udf1-f606w-o.fits
	mres=$$(astheader $(MUSEINPUTS)/muse-udf10-f606w.fits -h1          \
	                  | grep CD2_2 | awk '{print $$3}');               \
	hres=$$(astheader $(hdegdir)/udf1-f606w-o.fits | grep PC2_2        \
	                  | awk '{print $$3}');                            \
	echo | awk '{print '$$hres'/'$$mres'}' > $@





# HST images to MUSE resolution
# -----------------------------
#
# The HST images were convolved with the MUSE PSF for the same spatial
# resolution, now, we need to warp them to the MUSE pixel grid to
# easily use one segmentation map over both images.
h-to-m-pixres = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                         \
	          $(foreach f, $(filters), $(hdegdir)/udf$(uid)-$(f).fits) )
$(h-to-m-pixres): $(hdegdir)/%.fits: $(hdegdir)/%-c.fits                     \
	          $(hdegdir)/pix-res-scale.txt $(imgwarp)

        # Warp the HST image to the MUSE pixel scale, first find the
        # scale factor (sf), then warp the image.
	scalefactor=$$(cat $(hdegdir)/pix-res-scale.txt);                    \
	astimgwarp $< --scale=$$scalefactor -o$@ --numthreads=1





# One pixel kernel
# ----------------
#
# This kernel is created since in practice it means no convolution
# with NoiseChisel.
hsegdir = $(BDIR)/seg-hst
onepkernel = $(hsegdir)/one-pix-kernel.fits
$(onepkernel): | $(hsegdir)
	echo "1" > $(hsegdir)/one-pix-kernel.txt
	astconvertt $(hsegdir)/one-pix-kernel.txt -o$@
	rm $(hsegdir)/one-pix-kernel.txt





# Create the segmentation map
# ---------------------------
#
# The first thing we need to do is to create a segmentation map that
# will be fed into Gnuastro's MakeCatalog to generate the final
# catalog. Unfortunately as it currently stands, the MUSE-generated
# broad-band image has too many artifacts at low surface
# brightnesses. So if we want to do detection over it, we are going to
# miss a lot of the fainter objects. Thus, the convolved and scaled
# HST image is our only choice.
#
# However, the HST image doesn't have much noise left (because of the
# huge kernel). So we will be convolving it with a 1 pixel kernel
# (effectively no convolution), and then using much more looser
# NoiseChisel parameters to give a reasonable result.
hsegments = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                         \
	      $(foreach f, $(filters), $(hsegdir)/udf$(uid)-$(f).fits) )
$(hsegments): $(hsegdir)/%.fits: $(hdegdir)/%.fits $(onepkernel)         \
	     $(hdegdir)/pix-res-scale.txt  $(noisechisel)

        # Generate a segmentation map on the convolved and rescaled
        # HST image.
	astnoisechisel $< -o$(@D)/$*-nc.fits --kernel=$(onepkernel)      \
	               --minbfrac=0.0 --minmodeq=0.3 --qthresh=0.4       \
	               --dthresh=0.8 --detsnminarea=5 --minnumfalse=50   \
	               --segquant=0.5 --gthresh=1e10 --objbordersn=1e10

        # Make the clumps image an objects image. Note that because we
        # disabled growth and object separation, each "object" only
        # has one clump. So simply setting all the non-1 valued pixels
        # in the objects image to zero, will do the job for us.
	astarithmetic $(@D)/$*-nc.fits $(@D)/$*-nc.fits 1 neq 0 where    \
	              -h1 -h2 -o$@ --type=long





# Subtract the Sky value from the HST images
# ------------------------------------------
#
# Once NoiseChisel is run on the degraded HST images, we have the Sky
# value and we can subtract it from the input image to clean it up. As
# described in commit 526d8e9 (titled: `A description is written for
# the process and results'), the F814W image in particular shows
# strong sky residuals, so this step is necessary.
cutdir = $(BDIR)/cutouts
finalhstdeg = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                         \
	        $(foreach f, $(filters), $(cutdir)/udf$(uid)-$(f)-h.fits) )
$(finalhstdeg): $(cutdir)/%-h.fits: $(hsegdir)/%.fits | $(cutdir)

	astarithmetic $(hsegdir)/$*-nc.fits $(hsegdir)/$*-nc.fits - -h0 -h3 -o$@





# Demonstration of HST's bad sky subtraction
# ------------------------------------------
#
# Convert the un-skysubtracted images of the f814w and f850lp degraded
# HST images into PDF (with the same scaling) for demonstration.
h814demodir = $(BDIR)/tex/f814w-demo
deghst-demo = $(h814demodir)/udf1-f814w.pdf $(h814demodir)/udf1-f850lp.pdf
$(deghst-demo): $(h814demodir)/%.pdf: $(hdegdir)/%.fits | $(h814demodir)
	astconvertt $< -o$@ --fluxlow=-0.001 --fluxhigh=0.02 --noinvert
