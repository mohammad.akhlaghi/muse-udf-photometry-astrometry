# Build the report describing the pipeline. This text/figures is
# finally planned to be a section of the MUSE UDF paper.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# Build the tikz directory
# ------------------------
#
# TeX's TikZ tool (along with PGFPlots) is used to build all the
# images in the paper. In order to do its job fast and not have to
# rebuild the images on each run of LaTeX we are using its external
# feature so the image is rebuilt only when necessary. All the
# internal TikZ information for each figure is kept in this directory.
tikz:; mkdir tikz





# Merge all LaTeX macros
# ----------------------
#
# All the separate makefiles wrote their necessary information for the
# paper in the $(mtexdir) directory. Here, we will just concatenate
# all of them into one file to easily import into the LaTeX source of
# the paper.
#
# IMPORTANT NOTE: we cannot simply put `$(mtexdir)/*.tex' for the list
# of prerequisites because all these files are targets them selves and
# do not necessarily exist before Make actually starts (for example
# when the pipeline is run for the first time). You just have to add
# their root filename to the prerequisites list manually here.
tex/pipeline.tex: $(foreach t, versions statistics, $(mtexdir)/$(t).tex)
	echo "\\newcommand{\\buildtexdir}{$(BDIR)/tex}"       > $@
	cat $^                                               >> $@





# Necessary data-products
# -----------------------
#
# These data products are not directly related to TeX for making the
# final paper, but are never-the-less necessary to build. during the
# research. If only the paper is necessary, they can be avoided. They
# are dependencies of both the `.bbl' and `.pdf' final targets. This
# is due to the fact that the final PDF needs its references and it
# will call the `.bbl' rule, but only after the first build. So during
# the research, sometimes it is the `.bbl' rule that will run LaTeX
# first and sometimes its the `.pdf' rule, so `data-products' is a
# dependency in both cases.
data-products = $(radec2d) $(mag2d) $(deghst-demo) $(finaltarball)





# BibLaTeX references
# -------------------
#
# To build the BibLaTeX references, we need to run LaTeX once and then
# biber, the rule for building the final PDF will build the final PDF.
description.bbl: $(data-products) tex/ref.tex tex/pipeline.tex  | tikz

	if pdflatex -shell-escape -halt-on-error description.tex; then  \
	  echo "LaTeX (in preparation for BibLaTeX) is done.";          \
	else                                                            \
	  rm -f *.auxlock *.bcf *.run.xml *.blg *.aux *.log *.out;      \
	  exit 1;                                                       \
	fi;
	biber description.bcf
	pdflatex -shell-escape description.tex;





# Build final PDF
# ---------------
#
# Note that eventhough `tex/pipeline.tex' is in the `tex/' directory
# `tex/*' will not detect it necessarily because it might not be
# present (it is a target to be built by Make).
description.pdf: $(data-products) description.tex tex/* tex/pipeline.tex \
	         description.bbl | tikz

        # Delete some outputs for TeX to rebuild (if needed)
#	rm tikz/description-figure0*

        # Build the PDF
	if pdflatex -shell-escape -halt-on-error description.tex; then  \
	  echo "LaTeX is done.";                                        \
	else                                                            \
	  rm description.aux;                                           \
	  exit 1;                                                       \
	fi

        # Clean up
	rm -f description.log description.out description.bcf
	rm -f description.blg description.run.xml description.auxlock

        # Alert that the versions.tex file is not necessarily up to date
	@echo; echo; echo; echo "NOTE: Before sharing this PDF, make sure you have committed your changes, delete $(mtexdir)/versions.tex and run 'make' again so the PDF is stamped with the most recent version."
