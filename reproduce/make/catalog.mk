# Broad-band photometry checks with MUSE generated broad-band images.
#
# Generate the necessary catalogs for each region.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.








# Make a Sky and Sky_std image
# ----------------------------
#
# Since the purpose of this study is calibration, we don't want to
# subtract the Sky value. For the Sky value we will set all pixels to
# zero and for the STD we will set them all to 1.
ssdir = $(BDIR)/sky-and-std
skys-and-stds = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                    \
	          $(ssdir)/udf$(uid)-sky.fits)
genssname = $(ssdir)/$(word 1, $(subst -, ,$(1)))-$(2).fits
$(skys-and-stds): $(ssdir)/%-sky.fits: $(hsegdir)/%-f606w.fits | $(ssdir)

        # The segmentation map is only 0 and positive values, so if we
        # check the equality with a negative value, all the pixels
        # will be zero and if we check greater or equal to zero, then
        # all pixels will be 1.
	astarithmetic $<  0 ge -o$(ssdir)/$*-std.fits
	astarithmetic $< -1 eq -o$@





# Magnitude catalogs
# ------------------
#
# For the magnitude comparison we are using a segmentation map from
# the HST images (to go to fainter magnitudes). For the
# astrometry/position comparison (another rule) we will be using
# segmentation maps defined by MUSE.
#
# The MUSE zeropoint values are calculated based on the following:
#
# From the BUNIT keyword on MUSE data, the data have units of `10**-20
# Angstrom-1 cm-2 erg s-1'. However, the AB magnitude is defined like
# this (https://en.wikipedia.org/wiki/AB_magnitude):
#
#   M_AB = -2.5 * log10(f_n/Jy) + 8.90   (1)
#
# Where `f_n' is in units of `erg s-1 cm-2 Hz-1'. The conversion from
# flux in units of wavelength is (`L' is for lambda):
#
#   f_n/Jy = 3.34 * 10^4 * (L/Angstrom)^2 * f_l/(erg s-1 cm-2 A-1)
#
# So assuming `L' and `f_l' have been divided by their units and thus
# unitless (and `f_l' is the MUSE pixel units), we can say:
#
#   log10(f_n/Jy) = log10( 3.34 * 10^4 * (L)^2 * 10^(-20) * f_l )
#                 = log10( f_l ) + 2*log10( L ) - 15.4763
#
# So the AB magnitude of a measurement on the MUSE images is:
#
#   M_AB = -2.5 * [ log10( f_l ) + 2*log10( L ) - 15.4763 ] + 8.90
#        = -2.5*log10( f_l ) - 5*log10( L ) + 47.5908
#
# Hence, the zeropoint magnitude is: `47.5908 - 5*log10( L )'.
#
# Note on `genssname' and `genskyname': One segmentation map is
# generated for both HST and MUSE in each field and filter, also all
# filters of one field have the same Sky file name. These functions
# are used to generate the proper name from the output's name.
mcatdir = $(BDIR)/catalogs-magnitude
fieldfilter = $(word 1, $(subst -, ,$(1)))-$(word 2, $(subst -, ,$(1)))
mag-catalogs = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                       \
	         $(foreach f, $(filters),                                 \
	           $(foreach i, h m, $(mcatdir)/udf$(uid)-$(f)-$(i).txt) ) )
magsegname = $(hsegdir)/$(call fieldfilter, $(1)).fits
$(mag-catalogs): $(mcatdir)/%.txt: $(cutdir)/%.fits $$(call magsegname,%) \
	         $$(call genssname,%,sky) $(lambda-eff) $(mkcatalog)      \
	         | $(mcatdir)

        # Set the zeropoint value from the filter depending on the
        # instrument.
	filter=$(word 2, $(subst -, ,$*));                                \
	if [ $(lastword $(subst -, ,$*)) = "h" ]; then                    \
	  if   [ $$filter = "f606w"  ]; then zp=26.51;                    \
	  elif [ $$filter = "f775w"  ]; then zp=25.69;                    \
	  elif [ $$filter = "f814w"  ]; then zp=25.94;                    \
	  elif [ $$filter = "f850lp" ]; then zp=24.87;                    \
	  fi;                                                             \
	else                                                              \
	  zp=$$(awk '{print 47.5908 - 5*log($$1)/log(10)}'                \
	            $(leffdir)/$$filter".txt");                           \
	fi;                                                               \
	astmkcatalog $< --zeropoint=$$zp --magnitude --dec --ra -o$@      \
	             --objlabs=$(call magsegname, $*)        --objhdu=0   \
	             --skyfilename=$(call genssname, $*,sky) --skyhdu=0   \
	             --stdfilename=$(call genssname, $*,std) --stdhdu=0





# Astrometry catalogs
# -------------------
#
# We need to use the NoiseChisel segmentation maps derived from MUSE
# images for the astrometry/position catalogs.
acatdir = $(BDIR)/catalogs-astrometry
ast-catalogs = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                       \
	         $(foreach f, $(filters),                                 \
	           $(foreach i, h m, $(acatdir)/udf$(uid)-$(f)-$(i).txt) ) )
astsegname = $(msegdir)/$(call fieldfilter, $(1)).fits
$(ast-catalogs): $(acatdir)/%.txt: $(cutdir)/%.fits $$(call astsegname,%) \
	         $$(call genssname,%,sky) $(lambda-eff) $(mkcatalog)      \
	         | $(acatdir)

        # Set the zeropoint value from the filter depending on the
        # instrument.
	filter=$(word 2, $(subst -, ,$*));                                \
	if [ $(lastword $(subst -, ,$*)) = "h" ]; then                    \
	  if   [ $$filter = "f606w"  ]; then zp=26.51;                    \
	  elif [ $$filter = "f775w"  ]; then zp=25.69;                    \
	  elif [ $$filter = "f814w"  ]; then zp=25.94;                    \
	  elif [ $$filter = "f850lp" ]; then zp=24.87;                    \
	  fi;                                                             \
	else                                                              \
	  zp=$$(awk '{print 47.5908 - 5*log($$1)/log(10)}'                \
	            $(leffdir)/$$filter".txt");                           \
	fi;                                                               \
	astmkcatalog $< --zeropoint=$$zp --magnitude --dec --ra -o$@      \
	             --objlabs=$(call astsegname, $*)        --objhdu=0   \
	             --skyfilename=$(call genssname, $*,sky) --skyhdu=0   \
	             --stdfilename=$(call genssname, $*,std) --stdhdu=0





# Merge the MUSE and HST catalogs into one
# ----------------------------------------
#
# Put the necessary columns of the HST and MUSE magnitude catalogs
# into the same file and remove those that have NaN measurements.
samecat = $(BDIR)/catalogs-
merged-cats = $(foreach uid, 1 2 3 4 5 6 7 8 9 10,                          \
	        $(foreach f, $(filters),                                    \
                  $(foreach d, $(acatdir) $(mcatdir), $(d)/udf$(uid)-$(f).txt)))
$(merged-cats): $(samecat)%.txt: $(samecat)%-h.txt $(samecat)%-m.txt

        # First merge the two catalogs of the same field and filter
        # together into one file to make it easier to process them.
	paste $^ > $(samecat)$*-pasted.txt

        # Remove the comments and NaN magnitude results and keep the
        # differences between the MUSE and HST positions and
        # magnitudes.
	subfield=$(subst udf,,$(word 1, $(subst -, ,$(notdir $*))));        \
	awk 'BEGIN{                                                         \
	       print "# Reproduction pipeline $(gitdescribe).";             \
	       print "# Column 1: FIELD_ID      [label, us] MUSE UDF Field (UDF1, ..., UDF10).";         \
	       print "# Column 2: SEG_ID        [label, us] Segmentation map ID.";                       \
	       print "# Column 3: HST_RA        [degrees, d] Right Ascension in degraded HST image.";    \
	       print "# Column 4: HST_DEC       [degrees, d] Declination in degraded HST image.";        \
	       print "# Column 5: HST_MAG       [AB magnitude, f] Magnitude in degraded HST image.";     \
	       print "# Column 6: MUSE_DIFF_RA  [degrees, f] MUSE_RA - HST_RA";                          \
	       print "# Column 7: MUSE_DIFF_DEC [degress, f] MUSE_DEC - HST_DEC";                        \
	       print "# Column 8: MUSE_DIFF_MAG [AB magnitude, f] MUSE_MAG - HST_MAG";                   \
	     }                                                              \
	     !/^#/ && $$4!="nan" && $$8!="nan" {                            \
	       printf("%-4d%-5d%-14.8f%-14.8f%-8.3f%-14.8f%-14.8f%-8.3f\n", \
	              '$$subfield', $$1, $$2, $$3, $$4, $$6-$$2, $$7-$$3,   \
	              $$8-$$4) }' $(samecat)$*-pasted.txt > $@

        # Clean up.
	rm $(samecat)$*-pasted.txt





# Full field comparisons
# ----------------------
#
# The cleaned catalogs were for one sub-field in each filter, so now
# we will merge the nine UDF subfields into one file. Note that the
# UDF10 field doesn't need any merging of catalogs.
fullmosaic = $(foreach f, $(filters),                                  \
               $(foreach d, $(acatdir) $(mcatdir), $(d)/udf-$(f).txt) )
$(fullmosaic): $(samecat)%.txt: $(foreach i, 1 2 3 4 5 6 7 8 9,       \
	              $$(dir $$@)udf$(i)-$$(subst udf-,,$$(notdir $$*)).txt )

        # The comments are similar to all the catalogs, so just put
        # those of the first prerequisite.
	awk '/^#/{print}' $< > $@

        # Put all the non-commented rows of each subfield.
	for file in $^; do awk '!/^#/{print}' $$file >> $@; done





# All output tables in one tarball
outdir = reproduce/output
finaltardir  = $(BDIR)/mag-ast-comparison
finaltarball = $(outdir)/mag-ast-comp.tar.gz
$(outdir):; mkdir -p $(outdir)
$(finaltarball): $(fullmosaic) $(merged-cats) | $(outdir)

        # Directory to hold the final tables before compressing
	mkdir -p $(finaltardir)

        # Put all the final files in a single directory (while
        # correcting their name)
	for type in mag ast; do                                        \
	  if [ $$type = mag ]; then tdir=$(mcatdir);                   \
	  else                      tdir=$(acatdir);                   \
	  fi;                                                          \
                                                                       \
	  for u in udf udf10; do                                       \
	    for f in $(filters); do                                    \
	      cp $$tdir/$$u-$$f.txt $(finaltardir)/$$u-$$f-$$type.txt; \
	    done;                                                      \
	  done;                                                        \
	done

        # Make the final tarball and delete the single directory.
	c=$$(pwd); cd $(finaltardir); tar cf $$c/$@ *.txt
	rm -rf $(finaltardir)
