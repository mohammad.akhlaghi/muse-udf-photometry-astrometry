# Configuration files for programs

arithmetic = reproduce/config/gnuastro/astarithmetic.conf
convolve = reproduce/config/gnuastro/astconvolve.conf
header = reproduce/config/gnuastro/astheader.conf
imgcrop = reproduce/config/gnuastro/astimgcrop.conf
imgwarp = reproduce/config/gnuastro/astimgwarp.conf
mkcatalog = reproduce/config/gnuastro/astmkcatalog.conf
mkprof = reproduce/config/gnuastro/astmkprof.conf
noisechisel = reproduce/config/gnuastro/astnoisechisel.conf
