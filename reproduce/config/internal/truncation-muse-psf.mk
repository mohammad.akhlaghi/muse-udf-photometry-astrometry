# Truncation radius for generating the MUSE PSF
#
# This is in units of the FWHM.
truncation-muse-psf = 6
