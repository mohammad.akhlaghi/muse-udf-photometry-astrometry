# The pixel scale of the input images in units of 0.01 arcseconds
#
# The reason it is in units of 0.01 arcseconds is that we want to use
# it in the filenames and this makes it easy.
hstpixelscale = 6
