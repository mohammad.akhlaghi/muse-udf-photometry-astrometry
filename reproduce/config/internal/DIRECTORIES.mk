# Top-level user specific directories. Note the points below:
#
# - The VALUES to these directories are initially JUST PLACE-HOLDERS!
#   Please correct them based on your system.
#
# - The directories don't need to necessarily exist. If they do not
#   exist, they will be created and the necessary data will be
#   downloaded into them. Ofcourse provided that you have write
#   permissions and an internet connection.
#
# - Do not use the tilde expansion `~' or variables for your home
#   directory. Please use the full address, for example
#   `/home/your-user-name'.
#
# - An ending forward-slash `/' is NOT necessary. In the pipeline, all
#   these variables will be followed by a `/', so if you put a `/' at
#   the end of the value here, you will see a `//' in the printed
#   outputs during the processing. This has no technical problem, but
#   can make reading the outputs harder and is thus not recommended.





# Survey directories
# ------------------
#
# This is where the survey images and necessary files (with the same
# file-name standard as the main webpage) are stored. If this
# directory doesn't exist, or it doesn't contain the images (with the
# correct file-name formats), it will be created and the images will
# be downloaded. See `src/downloads.mk', for the URLs containing the
# expected inputs for each survey.
XDF = /path/to/xdf/survey/images
TROUGHPUTS = /path/to/hst/throughputs
MUSEINPUTS = /path/to/muse/pseudo/broad/band/images





# Build directory
# ---------------
#
# This is where the intermediate outputs of each step are kept.
#
# Why a separate build directory? So the source and configuration
# files for this reproduction pipeline do not get crowded by all the
# intermediate/derivative files. Also to make synchronization and
# backups more easy: the contents of the build directory do not need
# to be backed up since they can be reproduced and they can be large.
BDIR = /place/to/keep/intermediate/or/temporary/files





# Symbolic link to build directory
# --------------------------------
#
# If you want a symbolic link to the build directory in this top
# source directory then use this variable to name the symbolic
# link. If the value is empty (nothing after the `=' sign) then no
# symbolic link will not be created.
#
# Why a symbolic link? Because some users might prefer to have the top
# build directory can be in a completely separate filesystem. In this
# case, reaching it from within this directory might not be easy. With
# this symbolic link, it will be very easy to directly go to the top
# build directory, no matter where it is located.
BSYM = reproduce/build
