# UDF4 region
# ============
#
# Vertices of polygon to define region to use UDF4.

udf4raa  =  53.148436
udf4deca = -27.786683
udf4rab  =  53.136052
udf4decb = -27.776859
udf4rac  =  53.146975
udf4decc = -27.766372
udf4rad  =  53.159180
udf4decd = -27.776273

# The polygon option for ImageCrop used for these vertices.
udf4polygon = --polygon=$(udf4raa),$(udf4deca):$(udf4rab),$(udf4decb):$(udf4rac),$(udf4decc):$(udf4rad),$(udf4decd)
