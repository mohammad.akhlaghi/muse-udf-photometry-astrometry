# UDF9 region
# ============
#
# Vertices of polygon to define region to use UDF9.

udf9raa  =  53.162442
udf9deca = -27.821235
udf9rab  =  53.149729
udf9decb = -27.810759
udf9rac  =  53.161296
udf9decc = -27.799688
udf9rad  =  53.174245
udf9decd = -27.810176

# The polygon option for ImageCrop used for these vertices.
udf9polygon = --polygon=$(udf9raa),$(udf9deca):$(udf9rab),$(udf9decb):$(udf9rac),$(udf9decc):$(udf9rad),$(udf9decd)
