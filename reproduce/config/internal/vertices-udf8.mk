# UDF8 region
# ============
#
# Vertices of polygon to define region to use UDF8.

udf8raa  =  53.148098
udf8deca = -27.810511
udf8rab  =  53.136237
udf8decb = -27.800755
udf8rac  =  53.148071
udf8decc = -27.789328
udf8rad  =  53.160087
udf8decd = -27.798638

# The polygon option for ImageCrop used for these vertices.
udf8polygon = --polygon=$(udf8raa),$(udf8deca):$(udf8rab),$(udf8decb):$(udf8rac),$(udf8decc):$(udf8rad),$(udf8decd)
