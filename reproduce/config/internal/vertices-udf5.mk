# UDF5 region
# ============
#
# Vertices of polygon to define region to use UDF5.

udf5raa  =  53.160766
udf5deca = -27.797776
udf5rab  =  53.149576
udf5decb = -27.788774
udf5rac  =  53.161196
udf5decc = -27.777309
udf5rad  =  53.172679
udf5decd = -27.786412

# The polygon option for ImageCrop used for these vertices.
udf5polygon = --polygon=$(udf5raa),$(udf5deca):$(udf5rab),$(udf5decb):$(udf5rac),$(udf5decc):$(udf5rad),$(udf5decd)
