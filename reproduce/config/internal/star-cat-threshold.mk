# Only pixels above this multiple of the Sky standard deviation should
# be used in finding the center of the known star.
star-cat-threshold = 30
