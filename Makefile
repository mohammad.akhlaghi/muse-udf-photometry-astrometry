# Broad-band photometry checks with MUSE generated broad-band images.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.










# Configurations
# --------------
#
# These are the general configuration variables for a large number of
# independent rules, so they need to be included up here.

# Configuration parameters
include reproduce/config/internal/*.mk





# First goal is the default
# -------------------------
all: $(BSYM) description.pdf





# Enable secondary expansion
# --------------------------
#
# Secondary expansion allows usage of functions in the prerequisites.
.SECONDEXPANSION:





# Rules
# -----
#
# Note that we cannot simply include `reproduce/src/*.mk'. Because the
# order of reading them into Make actually matters in the case of
# variables especially. The names are mostly descriptive and following
# theses make files in the same order that they are defined here can
# help in understanding the full process.
include $(foreach m, preparations download hst muse catalog statistics    \
	             description, reproduce/make/$(m).mk)





# Phony targets
# -------------
#
# These are targets that do not correspond to a file (phony in the
# Make terminology). The following rules define what they do.
.PHONY: all clean





# Clean rules
# -----------
#
# To clean the outputs if necessary.
clean:
	rm -rf $(BDIR)/* $(BSYM) tikz tex/pipeline.tex *.aux *.bbl
